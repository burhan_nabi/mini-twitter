module UsersHelper
	
	def	gravatar_for(user)
		gravatar_ID = Digest::MD5::hexdigest(user.email.downcase)
		gravatar_URL = 
				"https://secure.gravatar.com/avatar/#{gravatar_ID}"
		image_tag(gravatar_URL, alt: "#{user.name}'s Image", 
							class: "gravatar")
	end
end
