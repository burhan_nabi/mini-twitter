module ApplicationHelper

	# Returns a full title on per page basis
	def full_title(page_title="")
			base_title = "Mini-Twitter"
			if( page_title.empty? )
				base_title
			else
				page_title + " | #{base_title}"
			end
	end	
end
